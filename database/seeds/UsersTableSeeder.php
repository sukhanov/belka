<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'first_name' => 'Александр',
                'last_name' => 'Пушкин',
                'email' => 'sasha.pushkin@gmail.com',
                'phone' => '79990000000',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'first_name' => 'Михаил',
                'last_name' => 'Лермонтов',
                'email' => 'lermont@gmail.com',
                'phone' => '79990000001',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ]);
    }
}
