<?php

use Illuminate\Database\Seeder;

class TripsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trips')->insert([
            [
                'rent_id' => 1,
                'rate_id' => 1,
                'created_at' => '2019-03-21 12:29:00',
                'updated_at' => '2019-03-21 14:00:00',
                'distance' => 24.5
            ],
            [
                'rent_id' => 2,
                'rate_id' => 1,
                'created_at' => '2019-03-21 12:20:00',
                'updated_at' => '2019-03-21 13:20:00',
                'distance' => 90.0
            ],
            [
                'rent_id' => 1,
                'rate_id' => 1,
                'created_at' => '2019-03-21 14:20:00',
                'updated_at' => '2019-03-21 14:45:00',
                'distance' => 20.0
            ],

        ]);
    }
}
