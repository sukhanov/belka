<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RentsOptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rents_options')->insert([
            [
                'rent_id' => 1,
                'option_id' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ]);
    }
}
