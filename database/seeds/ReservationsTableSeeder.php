<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ReservationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reservations')->insert([
            [
                'rent_id' => 1,
                'rate_id' => 3,
                'created_at' => '2019-03-21 12:00:00',
                'updated_at' => '2019-03-21 12:25:00',
            ],
            [
                'rent_id' => 2,
                'rate_id' => 3,
                'created_at' => '2019-03-21 12:00:00',
                'updated_at' => '2019-03-21 12:12:00',
            ]
        ]);
    }
}
