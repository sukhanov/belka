<?php

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{


    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call([
             GroupsTableSeeder::class,
             LoyaltyProgramsTableSeeder::class,
             UsersTableSeeder::class,
             UsersGroupsTableSeeder::class,
             LoyaltyProgramsTableSeeder::class,
             OptionsTableSeeder::class,
             CarClassesTableSeeder::class,
             CarsTableSeeder::class,
             RentsTableSeeder::class,
             RentsOptionsTableSeeder::class,
             TimeRatesTableSeeder::class,
             DistanceRatesTableSeeder::class,
             ReservationsTableSeeder::class,
             ExaminationsTableSeeder::class,
             TripsTableSeeder::class,
             ParkingsTableSeeder::class,
             UsersLoyaltyProgramsTableSeeder::class,
         ]);
    }
}
