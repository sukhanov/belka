<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rents')->insert([
            [
                'user_id' => 1,
                'car_id' => 2,
                'created_at' => '2019-03-21 12:00:00',
                'updated_at' => '2019-03-21 21:00:00',
                'status' => 'parking'
            ],
            [
                'user_id' => 1,
                'car_id' => 1,
                'created_at' => '2019-03-21 12:00:00',
                'updated_at' => '2019-03-22 01:00:00',
                'status' => 'parking'
            ],
        ]);
    }
}
