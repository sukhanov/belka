<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TimeRatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('time_rates')->insert([
            // Parking
            [
                'rate' => 2,
                'no_payment_since' => '23:00',
                'no_payment_till' => '07:00',
                'rent_free_period' => null,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            // Examination
            [
                'rate' => 2,
                'no_payment_since' => null,
                'no_payment_till' => null,
                'rent_free_period' => 7,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            // Reservation
            [
                'rate' => 2,
                'no_payment_since' => '23:00',
                'no_payment_till' => '07:00',
                'rent_free_period' => 20,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

        ]);
    }
}
