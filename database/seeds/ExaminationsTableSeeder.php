<?php

use Illuminate\Database\Seeder;

class ExaminationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('examinations')->insert([
            [
                'rent_id' => 1,
                'rate_id' => 2,
                'created_at' => '2019-03-21 12:25:00',
                'updated_at' => '2019-03-21 12:29:00',
            ],
            [
                'rent_id' => 2,
                'rate_id' => 2,
                'created_at' => '2019-03-21 12:12:00',
                'updated_at' => '2019-03-21 12:20:00',
            ]
        ]);
    }
}
