<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class UsersLoyaltyProgramsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_loyalty_programs')->insert([
            [
                'user_id' => 1,
                'loyalty_program_id' => 2,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

            [
                'user_id' => 1,
                'loyalty_program_id' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ]);
    }
}
