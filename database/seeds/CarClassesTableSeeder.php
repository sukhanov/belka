<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CarClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('car_classes')->insert([
            [
                'name' => 'Премиум класс',
                'rate' => 12,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Стандарт',
                'rate' => 8,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]

        ]);
    }
}
