<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class LoyaltyProgramsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('loyalty_programs')->insert([
            [
                'name' => 'Акция "Майские"',
                'description' => 'Подпишись на рассылку и получи скидку 10% в мае',
                'discount' => 10,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Скидка 15% постоянным клиентам',
                'description' => 'Описание акции',
                'discount' => 15,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ]);
    }
}
