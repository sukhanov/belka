<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DistanceRatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('distance_rates')->insert([
            [
                'name' => 'Базовый тариф',
                'rate' => 8,
                'day_limit' => 70,
                'over_limit_rate' => 10,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

        ]);
    }
}
