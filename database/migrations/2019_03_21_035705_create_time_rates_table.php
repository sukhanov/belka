<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('time_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->float('rate');
            $table->time('no_payment_since')->nullable();
            $table->time('no_payment_till')->nullable();
            $table->smallInteger('rent_free_period')->nullable();
            $table->timestamps();
            $table->index('created_at');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_rates');
    }
}
