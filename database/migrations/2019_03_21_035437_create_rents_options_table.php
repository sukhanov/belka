<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRentsOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rents_options', function (Blueprint $table) {
            $table->integer('rent_id');
            $table->integer('option_id');
            $table->timestamps();
            $table->primary(['rent_id', 'option_id'], 'rents_options_complex_primary');
            $table->foreign('rent_id')->references('id')->on('rents');
            $table->foreign('option_id')->references('id')->on('options');
            $table->index('created_at');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rents_options');
    }
}
