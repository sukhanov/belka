<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersLoyaltyProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_loyalty_programs', function (Blueprint $table) {
            $table->integer('user_id');
            $table->integer('loyalty_program_id');
            $table->timestamps();
            $table->primary(['user_id', 'loyalty_program_id'], 'users_loyalty_program_complex_key');
            $table->foreign('user_id', 'ulp_user_id_foreign')->references('id')->on('users');
            $table->foreign('loyalty_program_id', 'ulp_loyalty_program_id_fk')->references('id')->on('loyalty_programs');
            $table->index('created_at');
            $table->index('updated_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_loyalty_programs');
    }
}
