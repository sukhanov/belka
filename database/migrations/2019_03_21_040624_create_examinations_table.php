<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExaminationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('examinations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rent_id');
            $table->integer('rate_id');
            $table->boolean('status')->default(0);
            $table->timestamps();
            $table->foreign('rent_id', 'exm_rent_id_foreign')->references('id')->on('rents');
            $table->foreign('rate_id', 'exm_rate_id_foreign')->references('id')->on('time_rates');
            $table->index('created_at');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('examinations');
    }
}
