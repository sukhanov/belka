
##How to deploy
```
composer install
php artisan migrate
php artisan db:seed

```


##Request example
```
curl -X POST \
  http://belka.test/api/bill/1/calculate \
  -H 'Accept: application/json' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: be922176-ee8f-48dc-845b-0fd7ebcde22a' \
  -H 'cache-control: no-cache' \
  -d '{"rentId" : 1}'
  
```

##Response emample

```
{
    "success": true,
    "data": {
        "stages": [
            {
                "id": 1,
                "rent_id": 1,
                "rate_id": 3,
                "status": false,
                "created_at": "2019-03-21 12:00:00",
                "updated_at": "2019-03-21 12:25:00",
                "rate": {
                    "id": 3,
                    "rate": "2",
                    "no_payment_since": "23:00:00",
                    "no_payment_till": "07:00:00",
                    "rent_free_period": 20,
                    "created_at": "2019-03-22 07:43:14",
                    "updated_at": "2019-03-22 07:43:14"
                }
            },
            {
                "id": 1,
                "rent_id": 1,
                "rate_id": 2,
                "status": false,
                "created_at": "2019-03-21 12:25:00",
                "updated_at": "2019-03-21 12:29:00",
                "rate": {
                    "id": 2,
                    "rate": "2",
                    "no_payment_since": null,
                    "no_payment_till": null,
                    "rent_free_period": 7,
                    "created_at": "2019-03-22 07:43:14",
                    "updated_at": "2019-03-22 07:43:14"
                }
            },
            {
                "id": 1,
                "rent_id": 1,
                "rate_id": 1,
                "status": false,
                "distance": "24.5",
                "created_at": "2019-03-21 12:29:00",
                "updated_at": "2019-03-21 14:00:00",
                "rate": {
                    "id": 1,
                    "name": "Базовый тариф",
                    "day_limit": 70,
                    "over_limit_rate": "10",
                    "rate": "8",
                    "created_at": "2019-03-22 07:43:14",
                    "updated_at": "2019-03-22 07:43:14"
                }
            },
            {
                "id": 3,
                "rent_id": 1,
                "rate_id": 1,
                "status": false,
                "distance": "20",
                "created_at": "2019-03-21 14:20:00",
                "updated_at": "2019-03-21 14:45:00",
                "rate": {
                    "id": 1,
                    "name": "Базовый тариф",
                    "day_limit": 70,
                    "over_limit_rate": "10",
                    "rate": "8",
                    "created_at": "2019-03-22 07:43:14",
                    "updated_at": "2019-03-22 07:43:14"
                }
            },
            {
                "id": 1,
                "rent_id": 1,
                "rate_id": 1,
                "status": false,
                "created_at": "2019-03-21 14:00:00",
                "updated_at": "2019-03-21 14:20:00",
                "rate": {
                    "id": 1,
                    "rate": "2",
                    "no_payment_since": "23:00:00",
                    "no_payment_till": "07:00:00",
                    "rent_free_period": null,
                    "created_at": "2019-03-22 07:43:14",
                    "updated_at": "2019-03-22 07:43:14"
                }
            },
            {
                "id": 3,
                "rent_id": 1,
                "rate_id": 1,
                "status": true,
                "created_at": "2019-03-21 14:45:00",
                "updated_at": "2019-03-21 14:45:00",
                "rate": {
                    "id": 1,
                    "rate": "2",
                    "no_payment_since": "23:00:00",
                    "no_payment_till": "07:00:00",
                    "rent_free_period": null,
                    "created_at": "2019-03-22 07:43:14",
                    "updated_at": "2019-03-22 07:43:14"
                }
            }
        ],
        "options": [
            {
                "id": 1,
                "name": "Детское кресло",
                "rate": "2",
                "max_price": "300",
                "created_at": "2019-03-22 07:43:14",
                "updated_at": "2019-03-22 07:43:14",
           
            },
            {
                "id": 2,
                "name": "Рейлинги",
                "rate": "1",
                "max_price": "150",
                "created_at": "2019-03-22 07:43:14",
                "updated_at": "2019-03-22 07:43:14",
              
            }
        ],
        "tolal": 1260.9,
        "value": 1401,
        "discount": 15
    }
}
```

##Request example 2
```
curl -X POST \
  http://belka.test/api/rates \
  -H 'Accept: application/json' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: e121b18a-1e9f-4b91-85f6-a17fee9d38a0' \
  -H 'cache-control: no-cache' \
  -d '{"userId" : 1, "carId": 1}'
```

##Response emample 2
```
{
    "success": true,
    "data": {
        "user": {
            "id": 1,
            "first_name": "Александр",
            "last_name": "Пушкин",
            "email": "sasha.pushkin@gmail.com",
            "phone": "79990000000",
            "discount": [
                {
                    "id": 1,
                    "name": "Акция \"Майские\"",
                    "description": "Подпишись на рассылку и получи скидку 10% в мае",
                    "discount": 10
                },
                {
                    "id": 2,
                    "name": "Скидка 15% постоянным клиентам",
                    "description": "Описание акции",
                    "discount": 15
                }
            ],
            "groups": [
                {
                    "id": 2,
                    "name": "Постоянные клиенты",
                    "discount": 15
                }
            ]
        },
        "car": {
            "id": 1,
            "name": "Ford focus",
            "car_class_id": 2,
            "rate": "8",
            "car_class": {
                "id": 2,
                "name": "Стандарт",
                "rate": "8"
            }
        }
    }
}
```