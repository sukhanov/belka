<?php


namespace App\Classes;


interface BillInterface
{
    public function getCost();
    public function getTimeToPay();
}