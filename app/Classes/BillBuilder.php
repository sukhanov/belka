<?php

namespace App\Classes;

use App\Classes\Bill;


class BillBuilder
{
    private $bill;

    public function __construct()
    {
        $this->bill = new Bill;
    }

    public function addStage($stage)
    {
        $this->bill->addStage($stage);

        return $this->bill;
    }

    public function addOption($option)
    {
        $this->bill->addOption($option);

        return $this->bill;
    }

    public function addDiscount($discount)
    {
        $this->bill->addDiscount($discount);

        return $this->bill;
    }


    public function calculate()
    {
        $sum = 0;

        $duration = 0;

        foreach ($this->bill->stages as $stage){
            $sum += $stage->getCost();
            $duration += $stage->getTimeToPay();
        }

        foreach ($this->bill->options as $option){
            $sum += $option->getCost($duration);
        }

        $this->bill->setValue($sum);

        $discountSize = $sum * 10 / 100;

        $this->bill->setTotal($sum - $discountSize);

        return $this->bill;
    }
}

