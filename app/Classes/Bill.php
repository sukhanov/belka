<?php

namespace App\Classes;

use App\Classes\BillInterface;


class Bill
{

    public $stages = [];

    public $options = [];

    public $tolal = 0;

    public $value = 0;

    public $discount = 0;

    public function addStage(BillInterface $stage)
    {
        $this->stages[] = $stage;
        return $this;
    }

    public function addOption($stage)
    {
        $this->options[] = $stage;
        return $this;
    }

    public function setTotal($total)
    {
        $this->tolal = $total;
    }

    public function getTotal($total)
    {
        return $this->tolal;
    }

    public function addDiscount($discount)
    {
        $this->discount = $discount;
    }

    public function getDiscount($discount)
    {
        return $this->discount;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    public function getValue($value)
    {
        return $this->value;
    }


}