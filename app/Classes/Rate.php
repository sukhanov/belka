<?php

namespace App\Classes;


use App\Models\Car;
use App\Models\User;

class Rate
{

    public $user;

    public $car;

    public function setCar(Car $car)
    {
        $this->car = $car;
        $this->car->carClass;
        return $this;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        $this->user->discount = $user->discount;
        $this->user->groups = $user->groups;
        return $this;
    }

    public function get()
    {
        return $this;
    }





}