<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class CarClass extends Model
{

    public $table = 'car_classes';

    protected $hidden = ['pivot', 'created_at', 'updated_at'];



}
