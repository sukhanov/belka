<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Car extends Model
{
    protected $hidden = ['pivot', 'created_at', 'updated_at'];

    public function carClass()
    {
        return $this->belongsTo(CarClass::class);
    }
}
