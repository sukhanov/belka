<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Option extends  Model
{

    protected $hidden = ['pivot'];

    public function getCost($minutes)
    {
        return $minutes * (float) $this->rate;
    }
}
