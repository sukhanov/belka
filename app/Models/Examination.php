<?php

namespace App\Models;


class Examination extends Time
{

    public function getTimeToPay()
    {
        $period = $this->getPeriod();

        $rentFreePeriod = $this->rate->rent_free_period;

        $timeToPay = $period - $rentFreePeriod;

        return $timeToPay > 0 ? $timeToPay : 0;
    }

}
