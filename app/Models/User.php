<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class User extends  Model
{

    protected $hidden = ['pivot', 'created_at', 'updated_at'];

    public function discount()
    {
        return $this->belongsToMany(LoyaltyProgram::class, 'users_loyalty_programs');
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'users_groups');
    }



}
