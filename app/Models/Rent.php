<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use App\Classes\BillInterface;
use Carbon\Carbon;


class Rent extends Model
{

    public function examination()
    {
        return $this->hasOne(Examination::class);
    }

    public function reservation()
    {
        return $this->hasOne(Reservation::class);
    }

    public function trips()
    {
        return $this->hasMany(Trip::class);
    }

    public function parkings()
    {
        return $this->hasMany(Parking::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function options()
    {
        return $this->belongsToMany(Option::class,'rents_options');
    }


    public static function rules()
    {
        return [
            'rentId' => 'bail|required|integer|exists:rents,id',
        ];
    }

}
