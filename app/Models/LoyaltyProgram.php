<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class LoyaltyProgram extends  Model
{
    public $table = 'loyalty_programs';

    protected $hidden = ['pivot', 'created_at', 'updated_at'];

}
