<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use App\Classes\BillInterface;
use Carbon\Carbon;
use App\Models\TimeRate;

abstract class Time extends  Model implements BillInterface
{

    public function rate()
    {
        return $this->belongsTo(TimeRate::class);
    }

    public function getCreatedAt()
    {
        return Carbon::parse($this->created_at);
    }

    public function getUpdatedAt()
    {
        return Carbon::parse($this->updated_at);
    }

    public function getPeriod()
    {
        return $this->getUpdatedAt()->diffInMinutes($this->getCreatedAt());
    }


    public function getCost()
    {
       return $this->getTimeToPay() * (float) $this->rate->rate;
    }

    abstract public function getTimeToPay();
}
