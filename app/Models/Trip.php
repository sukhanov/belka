<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use App\Classes\BillInterface;
use Carbon\Carbon;


class Trip extends Model implements BillInterface
{

    public function rate()
    {
        return $this->belongsTo(DistanceRate::class);
    }

    public function getCreatedAt()
    {
        return Carbon::parse($this->created_at);
    }

    public function getUpdatedAt()
    {
        return Carbon::parse($this->updated_at);
    }

    public function getPeriod()
    {
        return $this->getUpdatedAt()->diffInMinutes($this->getCreatedAt());
    }

    public function getCost()
    {
        return $this->getPeriod() * (float) $this->rate->rate + $this->getExtraDistance() * (float) $this->rate->over_limit_rate;
    }

    public function getExtraDistance()
    {
        if ($this->rate->day_limit < $this->distance) {
            return $this->distance - $this->rate->day_limit;
        }

        return 0;
    }

    public function getTimeToPay()
    {
        return $this->getPeriod();

    }
}
