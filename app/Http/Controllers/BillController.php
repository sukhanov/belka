<?php

namespace App\Http\Controllers;

use App\Models\Rent;
use App\Classes\BillBuilder;
use App\Http\Requests\BillRequest;

class BillController extends Controller
{

    public function calculate(BillRequest $request)
    {
        $bill = $this->prepareBill(Rent::find($request->rentId));

        return response(['success' => true, 'data' => $bill]);
    }


   private function prepareBill(Rent $rent)
   {
       $bill = new BillBuilder;

        // Addind some options
       if ($rent->options){
           foreach ($rent->options as $option){
               $bill->addOption($option);
           }
       }

       $bill->addStage($rent->reservation)
            ->addStage($rent->examination);

       if ($rent->trips){
           foreach ($rent->trips as $trip){
               $bill->addStage($trip);
           }
       }

       if ($rent->parkings){
           foreach ($rent->parkings as $parking){
               $bill->addStage($parking);
           }
       }

       if ($rent->user->discount){
           $bill->addDiscount($rent->user->discount->max('discount'));
       }

       return $bill->calculate();
   }


}
