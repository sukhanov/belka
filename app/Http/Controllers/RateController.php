<?php

namespace App\Http\Controllers;


use App\Classes\Rate;
use App\Http\Requests\RateRequest;
use App\Models\User;
use App\Models\Car;

class RateController extends Controller
{

    public function index(RateRequest $request)
    {

        $rate = new Rate();
        $rate->setCar(Car::find($request->carId))
            ->setUser(User::find($request->userId));

        return response(['success' => true, 'data' => $rate->get()]);
    }





}
